ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

val akkaVersion = "2.8.0"
val akkaVersionHttp = "10.5.2"

lazy val root = (project in file("."))
  .settings(
    name := "TradeConversionServer",
      libraryDependencies ++= Seq(
        akkaHttp,
        sprayJson,
        akkaStream,
        akkaActor,
        logger,
        play_json
    )
  )

val akkaHttp = "com.typesafe.akka" %% "akka-http" % akkaVersionHttp
val sprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % akkaVersionHttp
val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
val akkaActor = "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion
val logger = "ch.qos.logback" % "logback-classic" % "1.4.7"
val play_json = "com.typesafe.play" %% "play-json" % "2.9.4"