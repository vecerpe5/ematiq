import actors.TradeActor
import actors.TradeActor.Command
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.http.scaladsl.Http
import akka.util.Timeout
import http.ServerRouter
import service.{Service, Service1, Service2}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
object ServerApp {
  private def startHttpServer(tradeActor: ActorRef[Command], host: String, port: Int)(implicit system: ActorSystem[_]): Unit = {
    implicit val ec: ExecutionContext = system.executionContext
    val router = new ServerRouter(tradeActor)
    val routes = router.routes

    val httpBindingFuture: Future[Http.ServerBinding] = Http().newServerAt(host, port).bind(routes)
    httpBindingFuture.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info(s"Server online at http:/$address")
      case Failure(exception) =>
        system.log.error(s"Failed to bind HTTP server, exception: $exception")
        system.terminate()
    }
  }

  def main(args: Array[String]): Unit = {
    trait RootCommand
    case class RetrieveTradeActor(replyTo: ActorRef[ActorRef[Command]]) extends RootCommand

    val host: String = "127.0.0.1"
    val port: Int = 8080
    val APIS: Seq[Service] = Seq(Service1, Service2)

    val rootBehavior: Behavior[RootCommand] = Behaviors.setup { context =>
      val tradeActor = context.spawn(TradeActor(APIS), "TradeActor")
      Behaviors.receiveMessage {
        case RetrieveTradeActor(replyTo) =>
          replyTo ! tradeActor
          Behaviors.same
      }
    }

    implicit val system: ActorSystem[RootCommand] = ActorSystem(rootBehavior, "ServerSystem")
    implicit val timeout: Timeout = Timeout(5.seconds)
    implicit val ec: ExecutionContext = system.executionContext

    val tradeActorFuture: Future[ActorRef[Command]] = system.ask(replyTo => RetrieveTradeActor(replyTo))
    tradeActorFuture.map(tradeActor => startHttpServer(tradeActor, host, port))
  }
}
