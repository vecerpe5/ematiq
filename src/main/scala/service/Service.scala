package service

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpEntity, HttpRequest, HttpResponse}
import http.Trade

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

trait Service {
  protected def createHttpRequest(trade: Trade): HttpRequest
  protected def parseStingToBigDecimal(response: String): BigDecimal

  def fetchExchangeRate(trade: Trade)(implicit system: ActorSystem[_]): Future[BigDecimal] = {
    implicit val executionContextRef: ExecutionContext = system.executionContext

    val request = createHttpRequest(trade)
    val httpResponseFuture: Future[HttpResponse] = Http().singleRequest(request)
    val futureEntity: Future[HttpEntity.Strict] = httpResponseFuture.flatMap(response => response.entity.toStrict(100.millis))
    val futureExchangeRate: Future[BigDecimal] = futureEntity.map(entity => parseStingToBigDecimal(entity.data.utf8String))
    futureExchangeRate
  }
}