package service

import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import http.Trade
import play.api.libs.json.Json

import java.time.LocalDate

object Service2 extends Service {
  protected override def createHttpRequest(trade: Trade): HttpRequest = {
    val date: LocalDate = trade.date.toLocalDate
    val url = s"https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/$date/currencies/${trade.currency.toLowerCase()}/eur.json"
    HttpRequest(method = HttpMethods.GET, uri = url)
  }

  protected override def parseStingToBigDecimal(response: String): BigDecimal = {
    val json = Json.parse(response)
    val exchangeRate = (json \ "eur").as[BigDecimal]
    exchangeRate
  }
}
