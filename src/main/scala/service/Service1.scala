package service

import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import http.Trade
import play.api.libs.json.Json

import java.time.LocalDate

object Service1 extends Service {
  protected override def createHttpRequest(trade: Trade): HttpRequest = {
    val date: LocalDate = trade.date.toLocalDate
    val url = s"https://api.exchangerate.host/convert?from=${trade.currency}&to=EUR&date=$date"
    HttpRequest(method = HttpMethods.GET, uri = url)
  }

  protected override def parseStingToBigDecimal(response: String): BigDecimal  = {
    val json = Json.parse(response)
    val exchangeRate = (json \ "info" \ "rate").as[BigDecimal]
    exchangeRate
  }
}
