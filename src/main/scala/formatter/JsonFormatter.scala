package formatter

import http.Trade
import spray.json.DefaultJsonProtocol._
import spray.json._

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object JsonFormatter extends Formatter {
  implicit val localDateTimeFormat: RootJsonFormat[LocalDateTime] = new RootJsonFormat[LocalDateTime] {
    override def read(json: JsValue): LocalDateTime = json match {
      case JsString(value) => LocalDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
      case _ => deserializationError("Expected ISO date-time format")
    }
    override def write(obj: LocalDateTime): JsValue = JsString(obj.toString)
  }
  implicit val tradeFormat: RootJsonFormat[Trade] = jsonFormat6(Trade)
  override def fromMessage(message: String): Trade = message.parseJson.convertTo[Trade]
  override def toMessage(trade: Trade): String = trade.toJson.prettyPrint
}
