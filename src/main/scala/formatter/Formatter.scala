package formatter

import akka.http.scaladsl.model.{ContentType, ContentTypes}
import http.Trade

trait Formatter {
  def fromMessage(message: String): Trade
  def toMessage(value: Trade): String
}

object Formatter {
  def createMessageFormatter(contentType: ContentType): Formatter = contentType match {
    case ContentTypes.`application/json` => JsonFormatter
    case _ => throw new IllegalArgumentException("Currently unsupported Content-Type")
  }
}
