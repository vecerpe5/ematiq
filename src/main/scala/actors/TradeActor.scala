package actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import http.Trade
import service._

import scala.collection.mutable
import scala.concurrent._
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}
object TradeActor {
  sealed trait Command
  object Command {
    case class GetExchangeRate(trade: Trade, replyTo: ActorRef[Response]) extends Command
    case class DeleteExchangeRate(trade: Trade) extends Command
  }

  sealed trait Response

  object Response {
    case class TradeResponse(trade: Trade) extends Response
    case class FailedResponse() extends Response
  }

  import Command._
  import Response._

  def apply(APIs: Seq[Service]): Behavior[Command] = Behaviors.setup { context =>
    context.log.info("TradeActor created!")
    tradeStorageBehavior(mutable.Map.empty, APIs)
  }

  private def tradeStorageBehavior(cache: mutable.Map[String, Promise[Trade]], APIs: Seq[Service]): Behaviors.Receive[Command] =
    Behaviors.receive { (context, message) =>
      implicit val system: ActorSystem[_] = context.system
      implicit val executionContext: ExecutionContext = system.executionContext

      def callApis(services: Seq[Service], trade: Trade): Future[Trade] = {
        services match {
          case headService :: remainingServices =>
            headService.fetchExchangeRate(trade).transformWith {
              case Success(exchangeRate) =>
                system.log.info(s"External API ${headService.getClass.toString} success!")
                Future.successful(trade.exchange(exchangeRate))
              case Failure(exception) =>
                system.log.warn(s"Service not responding error: ${exception.getMessage}")
                callApis(remainingServices, trade)
            }
          case Nil => Future.failed(new TimeoutException("APIs are unreachable"))
        }
      }

      message match {
        case GetExchangeRate(trade, replyTo) =>
          context.log.info(s"GetExchangeRate: Trade = ${trade.key}")

          cache.get(trade.key) match {
            case Some(promise) =>
              context.log.info("Cache Hit")
              promise.future.onComplete {
                case Success(updatedTrade) => replyTo ! TradeResponse(updatedTrade)
                case Failure(exception) =>
                  system.log.error(exception.getMessage)
                  replyTo ! FailedResponse()
              }
            case _ =>
              context.log.info("Calling external APIs")
              val promise = Promise[Trade]
              cache.put(trade.key, promise)

              callApis(APIs, trade).onComplete {
                case Success(updatedTrade) =>
                  promise.success(updatedTrade)
                  context.scheduleOnce(2.hours, context.self, DeleteExchangeRate(trade))
                  replyTo ! TradeResponse(updatedTrade)
                case Failure(exception) =>
                  system.log.error(exception.getMessage)
                  promise.failure(exception)
                  cache.remove(trade.key)
                  replyTo ! FailedResponse()
              }
          }
          Behaviors.same

        case DeleteExchangeRate(trade) =>
          system.log.info(s"DeleteExchangeRate: Trade = ${trade.key}")
          cache.remove(trade.key)
          tradeStorageBehavior(cache, APIs)
      }
    }
}
