package http

import actors.TradeActor.Command.GetExchangeRate
import actors.TradeActor.Response.TradeResponse
import actors.TradeActor.{Command, Response}
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import formatter.Formatter

import java.time.LocalDateTime
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.math.BigDecimal.RoundingMode
import scala.util.Success


case class Trade(market: Int, selectionId: Int, odds: BigDecimal, stake: BigDecimal, currency: String, date: LocalDateTime) {
  def exchange(rate: BigDecimal, updatedCurrency: String = "EUR"): Trade = {
    val updatedStake = (stake * rate).setScale(5, RoundingMode.HALF_UP)
    copy(currency = updatedCurrency, stake = updatedStake)
  }
  def getExchangeRateCommand(replyTo: ActorRef[Response]): Command = GetExchangeRate(this, replyTo)
  def key: String = date.toLocalDate + currency.toUpperCase()
}

class ServerRouter(tradeActor: ActorRef[Command])(implicit val system: ActorSystem[_]) {
  private implicit val timeout: Timeout = Timeout(2.seconds)
  private def fetchExchangeRate(tradeRequest: Trade): Future[Response] = tradeActor.ask(replyTo => tradeRequest.getExchangeRateCommand(replyTo))

  val routes: Route = {
    pathPrefix("api" / "v1" / "conversion" / "trade") {
      pathEndOrSingleSlash {
        post {
          extractRequest { request =>
            val messageFormatter = Formatter.createMessageFormatter(request.entity.contentType)

            entity(as[String]) { message =>
              val trade = messageFormatter.fromMessage(message)

              onComplete(fetchExchangeRate(trade)) {
                case Success(TradeResponse(updatedTrade)) =>
                  val responseMessage: String = messageFormatter.toMessage(updatedTrade)
                  val exchangeRate = (updatedTrade.stake / trade.stake).setScale(5, RoundingMode.HALF_UP)
                  system.log.info(s"Response: Date: ${updatedTrade.date.toLocalDate} Currency: ${trade.currency} -> ${updatedTrade.currency} ExchangeRate: $exchangeRate")
                  complete(responseMessage)
                case _ =>
                  complete(StatusCodes.InternalServerError)
              }
            }
          }
        }
      }
    }
  }
}
